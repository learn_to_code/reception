#!/usr/bin/env ruby

visitors = []

def user_interaction
  puts 'Enter persons number to remove'
  puts 'or enter name to add new'
  puts 'or enter "q" to quit'
  response = STDIN.gets.chomp
  if response =~ /^\d+$/
    return response.to_i
  else
    return response
  end
end

def print_state(state)
  puts
  puts 'Currently inside:'
  state.each_with_index do |person, index|
    puts "#{index} - #{person}"
  end
end

continue = true
while continue
  print_state visitors
  response = user_interaction
  if response == 'q'
    continue = false
  elsif response.is_a? Fixnum
    visitors.delete_at response
  else
    visitors << response
  end
end
